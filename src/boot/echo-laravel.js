import Vue from 'vue'

import VueEcho from 'vue-echo-laravel';
window.io = require('socket.io-client');

Vue.use(VueEcho, {
  broadcaster: 'socket.io',
  host: 'https://api-likemytop.progim.net:6001',
});

import Echo from "laravel-echo"
import {LocalStorage} from "quasar";

window.io = require('socket.io-client');

export default ({app, router, Vue, store}) => {
  const token = LocalStorage.getItem('token');

  Vue.prototype.$echo = new Echo({
    broadcaster: 'socket.io',
    host: 'https://api-likemytop.progim.net:6001',
    authEndpoint: `/broadcasting/auth`,
    auth: {
      headers: {
        Accept: 'application/json',
        Authorization: token ? `Bearer ${token.token}` : '',
      }
    },
  });
}
