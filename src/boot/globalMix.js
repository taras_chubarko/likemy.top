import {createHelpers} from 'vuex-map-fields'
import {mapActions} from 'vuex'

const {mapFields} = createHelpers({
  getterType: 'projectFields',
  mutationType: 'updateProjectField',
});

export default async ({app, router, Vue}) => {
  //Vue.mixins(mainMixin);
  Vue.mixin({
    computed: {
      ...mapFields('main', {
        $showTopMenu: 'showTopMenu',
        $showTopMenuAlways: 'showTopMenuAlways',
      }),
      ...mapFields('movies', {
        $movie: 'movie',
        $comments: 'comments',
      }),
      ...mapFields('user', {
        $user: 'user',
        $profile: 'profile',
      }),
    },
    methods: {
      ...mapActions('main', ['showTopMenu']),

      $valid: function ($v, $messages) {
        const promise = new Promise((resolve, reject) => {
          $v.form.$touch();
          let arr = [];
          if ($v.form.$error) {
            _.forEach($v.form.$params, (v, k) => {
              if ($v.form[k].$error) {
                _.forEach($v.form[k].$params, (vv, kk) => {
                  if (kk === 'required') {
                    arr.push(`<li>"${$messages[k].required}" обязательно.</li>`);
                  }
                })
              }
            })

            this.$q.notify({
              message: '<ul>' + arr.join('') + '</ul>',
              color: 'negative',
              html: true,
            });
            reject(false)
          }
          resolve(true)
        });
        return promise;
      },


    },
  });
}
