import {ApolloClient} from 'apollo-client';
import {HttpLink, createHttpLink} from 'apollo-link-http';
import {ApolloLink} from 'apollo-link';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {LocalStorage, Notify} from 'quasar'
import {onError} from 'apollo-link-error';
import bus from '../boot/bus'

const customFetch = (uri, options) => {
  const token = LocalStorage.getItem('token');
  const user = LocalStorage.getItem('user');
  options.headers.u = LocalStorage.has('user') ? user.id : null;
  options.headers.Authorization = token ? `Bearer ${token.token}` : '';
  return fetch(uri, options);
};

const httpLink = createHttpLink({ uri: JSON.parse(process.env.API) + '/graphql/secret', fetch: customFetch });

const errorLink = onError(({graphQLErrors, networkError, operation, forward}) => {

  if (graphQLErrors && networkError && networkError.statusCode === 401) {
      Notify.create({
        message: 'Необходимо авторизоваться!',
        color: 'negative',
        html: true,
      });
      bus.$emit('auth');
  }else {
    if(graphQLErrors.length){
      const err = graphQLErrors[0];
      if (err.message === 'Unauthorized') {
        bus.$emit('forbidden');
      }
      if (err.message === 'validation') {
        handleValidationError(err);
      }
      if (err.message === 'Internal server error') {
        handleDebugError(err);
      }
    }

  }

});

export default new ApolloClient({
  link: errorLink.concat(httpLink),
  cache: new InMemoryCache({
    addTypename: false,
    dataIdFromObject: obj => obj.id,
    fragmentMatcher: {
      match: ({id}, typeCond, context) => !!context.store.get(id)
    }
  })
});

const handleValidationError = (error) => {
  //console.log(error.validation);
  let arr = [];
  Object.keys(error.validation).map(key => {
    let val = error.validation[key];
    val.forEach(item => {
      arr.push('<li>'+item+'</li>');
    })
  })

  if(arr.length){
    Notify.create({
      message: '<ul>'+arr.join('')+'</ul>',
      color: 'negative',
      html: true,
    });
  }
}

const handleDebugError = (error) => {
  let exepts = ['changeLike'];
  if(!exepts.includes(error.debugMessage)){
    Notify.create({
      message: error.debugMessage,
      color: 'negative',
    });
  }
}


