import {ApolloClient} from 'apollo-client';
import {ApolloLink} from 'apollo-link';
import {HttpLink, createHttpLink} from 'apollo-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {onError} from 'apollo-link-error';
import {LocalStorage, Notify} from 'quasar'
import bus from '../boot/bus'

// const httpLink = new HttpLink({uri: JSON.parse(process.env.API) + '/graphql'});
const customFetch = (uri, options) => {
  const user = LocalStorage.getItem('user');
  options.headers.u = LocalStorage.has('user') ? user.id : null;
  return fetch(uri, options);
};

const httpLink = createHttpLink({ uri: JSON.parse(process.env.API) + '/graphql', fetch: customFetch });

const errorLink = onError(({graphQLErrors, networkError, operation, forward}) => {
 // console.log(networkError.statusCode)
  if(graphQLErrors !== undefined && graphQLErrors.length){
    const err = graphQLErrors[0];
    if (err.message === 'validation') {
      handleValidationError(err);
    }
    if (err.message === 'Internal server error') {
      handleDebugError(err);
    }
  }
  return forward(operation);
});


export default new ApolloClient({
  link: errorLink.concat(httpLink),
  cache: new InMemoryCache({
    addTypename: false,
    dataIdFromObject: obj => obj.id,
    fragmentMatcher: {
      match: ({id}, typeCond, context) => !!context.store.get(id)
    }
  }),
});

const handleValidationError = (error) => {
  //console.log(error.validation);
  let arr = [];
  Object.keys(error.validation).map(key => {
    let val = error.validation[key];
    val.forEach(item => {
      arr.push('<li>'+item+'</li>');
    })
  })

  if(arr.length){
    Notify.create({
      message: '<ul>'+arr.join('')+'</ul>',
      color: 'negative',
      html: true,
    });
  }
}

const handleDebugError = (error) =>{
  if(error.debugMessage === 'Необходимо авторизоваться'){
    bus.$emit('auth');
  }
  Notify.create({
    message: error.debugMessage,
    color: 'negative',
  });
}
