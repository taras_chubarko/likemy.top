import Vue from 'vue';
const HelloJs = require('hellojs/dist/hello.all.min.js');
const VueHello = require('vue-hellojs');

HelloJs.init({
  google: '82343565587-t1lj4kdcemq2h81c16olnkitdtle67jr.apps.googleusercontent.com',
  facebook: 210131743718670,
  yandex: 'c73d58ee68954cecba2aabc64674f88b',
  vk: '7550995',
}, {
  redirect_uri: 'authcallback/'
});
Vue.use(VueHello, HelloJs);
