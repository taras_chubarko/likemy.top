//loadScript
import LoadScript from 'vue-plugin-load-script';

export default async ({ app, router, Vue }) => {
  Vue.use(LoadScript);
}
