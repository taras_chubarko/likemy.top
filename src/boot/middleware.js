//middleware
export default ({ router, store, Vue }) => {
  router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (Object.keys(store.state.user.token).length) {
        next()
      } else {
        next({
          path: '/403',
        })
      }
    }
    else if (to.matched.some(record => record.meta.guest)) {
      if (store.state.admin.auth) {
        next({
          path: '/',
        })
      } else {
        next()
      }
    }
    else {
      next() // make sure to always call next()!
    }
  })
}
