import VueMq from 'vue-mq'

export default ({ Vue }) => {
  Vue.use(VueMq,{
    breakpoints: { // default breakpoints - customize this
      xs: 599,
      sm: 1023,
      mdl: 1340,
      md: 1439,
      lg: 1919,
      xl: Infinity,
    }
  })
}
