//socauth
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueSocialauth from 'vue-social-auth'
import axios from 'axios';
Vue.use(VueAxios, axios)

Vue.use(VueSocialauth, {
  providers: {
    facebook: {
      clientId: '210131743718670',
      redirectUri: '/authcallback/' // Your client app URL
    },
    google: {
      clientId: '82343565587-t1lj4kdcemq2h81c16olnkitdtle67jr.apps.googleusercontent.com',
      redirectUri: '/authcallback/' // Your client app URL
    },
    vk: {
      clientId: '7550995',
      redirectUri: '/authcallback/' // Your client app URL
    },
    yandex: {
      clientId: 'c73d58ee68954cecba2aabc64674f88b',
      redirectUri: '/authcallback/' // Your client app URL
    },
  }
})
