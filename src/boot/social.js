const providers = {
  facebook: {
    client_id: '757496755012532',
    redirect_uri: process.env.BASE_URL + '/cb/facebook'
  },
  yandex: {
    client_id: '9450711b57364fa0b1dc425462d5f562',
    redirect_uri: process.env.BASE_URL + '/cb/yandex'
  },
  google: {
    client_id: '655972310277-97ipq9mdp7h1a2fv97tl0sdatqkvecej.apps.googleusercontent.com',
    redirect_uri: process.env.BASE_URL + '/cb/google'
  },
  mailru: {
    client_id: '336b2108d2b34abbb59ef02a1c54345e',
    redirect_uri: process.env.BASE_URL + '/cb/mailru'
  },
  vkontakte: {
    client_id: '7612578',
    redirect_uri: process.env.BASE_URL + '/cb/vkontakte'
  }
};


export default function socAuth(network) {
  const promise = new Promise((resolve, reject) => {
    let newWindow = null;
    if (network === 'facebook') {
      newWindow = window.open('https://www.facebook.com/v8.0/dialog/oauth?client_id=' + providers[network].client_id + '&redirect_uri=' + providers[network].redirect_uri + '&response_type=code&scope=email', "", "width=600,height=600");
    }
    if (network === 'yandex') {
      newWindow = window.open('https://oauth.yandex.ru/authorize?response_type=code&client_id=' + providers[network].client_id, "", "width=600,height=600");
    }
    if (network === 'google') {
      newWindow = window.open('https://accounts.google.com/o/oauth2/auth?redirect_uri='+ providers[network].redirect_uri +'&response_type=code&client_id=' + providers[network].client_id + '&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile', "", "width=600,height=600");
    }
    if (network === 'mailru') {
      newWindow = window.open('https://oauth.mail.ru/login?client_id='+ providers[network].client_id +'&response_type=code&scope=userinfo&redirect_uri='+ providers[network].redirect_uri +'&state=123', "", "width=600,height=600");
    }
    if (network === 'vkontakte') {
      newWindow = window.open('http://oauth.vk.com/authorize?client_id='+ providers[network].client_id +'&response_type=code&redirect_uri='+ providers[network].redirect_uri, "", "width=600,height=600");
    }

    let poolingInterval = setInterval(() => {
      //console.log(newWindow)
      if (!newWindow || newWindow.closed || newWindow.closed === undefined) {
        clearInterval(poolingInterval)
        poolingInterval = null
        newWindow = null
      }
      try {
        let uri = newWindow.location.origin+newWindow.location.pathname;
        //console.log(uri)
        if(uri === providers[network].redirect_uri){
          const urlParams = new URLSearchParams(newWindow.location.search);
          const code = urlParams.get('code');
          //console.log(code)
            resolve({
              network: network,
              code: code,
              redirect_uri: providers[network].redirect_uri
            })
          clearInterval(poolingInterval)
          poolingInterval = null
          newWindow.close();
          newWindow = null
        }
      }catch(e) {

      }

    }, 250);

  });
  return promise;
}

