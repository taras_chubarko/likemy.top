import {mapActions} from 'vuex'
import {createHelpers} from 'vuex-map-fields'

const {mapFields} = createHelpers({
  getterType: 'projectFields',
  mutationType: 'updateProjectField',
});

export const messages = {

  data(){
    return{
      q: {
        slug: this.$route.params.slug,
        per_page: 15,
        current_page: 1,
        search: null,
      }
    }
  },

  computed: {
    ...mapFields('messages', ['open', 'message', 'list', 'chatMessages', 'messagesUnread'])
  },

  methods: {
    ...mapActions(
        'messages',
        [
          'messageAddTrash',
          'getInbox',
          'getSent',
          'getStarred',
          'getMessage',
          'getChats',
          'getMessagesUnread',
          'getChatMessages',
          'messageAdd',
        ]),
    deleteMessages(ids) {
      //console.log(ids)
      this.messageAddTrash(ids).then(response => {
        this.open = false;
        if (this.$route.name === 'messages.slug') {
          this.getInbox(this.q);
        }
        if (this.$route.name === 'messages.slug.chats') {
          this.getChats(this.q);
        }
        if (this.$route.name === 'messages.slug.starred') {
          this.getStarred(this.q);
        }
      });
    },
  },

}
