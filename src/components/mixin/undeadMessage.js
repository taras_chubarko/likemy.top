import {mapActions} from 'vuex'

export const unreadMessage = {

  data() {
    return {
      countUnread: 0,
      polling: null
    }
  },

  methods:{
    ...mapActions('messages', ['getUnreadMessages']),
    // pollData () {
    //   this.polling = setInterval(() => {
    //     this.getUnreadMessages().then(response => {
    //       this.countUnread = response;
    //     })
    //   }, 3000)
    // }
  },

  beforeDestroy () {
    clearInterval(this.polling)
  },

  created () {
    if(Object.keys(this.$user).length){
      this.getUnreadMessages().then(response => {
        this.countUnread = response;
      })
      //this.pollData()
    }
  }

}
