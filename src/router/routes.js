
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'mail/activation/:id', component: () => import('pages/user/PageEmailActivation.vue') },
      { path: 'movie/:slug', name: 'movie.slug', component: () => import('pages/movies/PageMovie.vue') },
      { path: 'movie/actor/:id', name: 'movie.actor', component: () => import('pages/movies/PageActor.vue') },
      { path: 'collections', name: 'collections', component: () => import('pages/collections/PageCollections.vue') },
      { path: 'collections/:slug', name: 'collections.slug', component: () => import('pages/collections/PageCollectionsSlug.vue') },
      { path: 'catalog/:catalog/:sub_catalog', name: 'catalog', component: () => import('pages/catalog/PageCatalog.vue') },
      { path: 'catalog/:catalog', name: 'catalog.index', component: () => import('pages/catalog/PageCatalog.vue') },
      { path: 'movies/ff/:type', name: 'movie.ff', component: () => import('pages/movies/PageMovieFF.vue') },
      {
        path: 'user/:slug',
        meta: {requiresAuth: true},
        component: () => import('pages/user/profile/PageUserProfile.vue'),
        children: [
          { path: '', name: 'user.slug', component: () => import('pages/user/profile/PageUserProfilePreference.vue') },
          { path: 'likes', name: 'user.slug.likes', component: () => import('pages/user/profile/PageUserProfileLikes.vue') },
          { path: 'reviews', name: 'user.slug.reviews', component: () => import('pages/user/profile/PageUserProfileReviews.vue') },
          { path: 'followers', name: 'user.slug.followers', component: () => import('pages/user/profile/PageUserProfileFollowers.vue') },
          { path: 'followings', name: 'user.slug.followings', component: () => import('pages/user/profile/PageUserProfileFollowings.vue') },
        ]
      },
      {
        path: 'feed/:slug',
        meta: {requiresAuth: true},
        component: () => import('pages/user/feed/PageUserFeed.vue'),
        children: [
          { path: '', name: 'feed.slug', component: () => import('pages/user/feed/PageUserFeedNews.vue') },
          { path: 'chosen', name: 'feed.slug.chosen', component: () => import('pages/user/feed/PageUserFeedChosen.vue') },
          { path: 'followers', name: 'feed.slug.followers', component: () => import('pages/user/feed/PageUserFeedFollewers.vue') },
          { path: 'followings', name: 'feed.slug.followings', component: () => import('pages/user/feed/PageUserFeedFollowing.vue') },
          { path: 'search', name: 'feed.slug.search', component: () => import('pages/user/feed/PageUserFeedSearch.vue') },
        ]
      },
      {
        path: 'messages/:slug',
        meta: {requiresAuth: true},
        component: () => import('pages/user/messages/PageUserMessages.vue'),
        children: [
          { path: '', name: 'messages.slug', component: () => import('pages/user/messages/PageUserMessagesInbox.vue') },
          { path: 'sent', name: 'messages.slug.sent', component: () => import('pages/user/messages/PageUserMessagesSent.vue') },
          { path: 'starred', name: 'messages.slug.starred', component: () => import('pages/user/messages/PageUserMessagesStarred.vue') },
          { path: 'trash', name: 'messages.slug.trash', component: () => import('pages/user/messages/PageUserMessagesTrash.vue') },
          { path: 'chats', name: 'messages.slug.chats', component: () => import('pages/user/messages/PageUserChats.vue') },
          { path: 'chats/:id', name: 'messages.slug.chats.id', component: () => import('pages/user/chats/PageUserChatsChatID.vue') },
        ]
      },
      {
        path: 'chats/:slug',
        meta: {requiresAuth: true},
        component: () => import('pages/user/chats/PageUserChatsMain.vue'),
        children: [
          { path: ':id', name: 'chats.slug.id', component: () => import('pages/user/chats/PageUserChatsChatID.vue') },
        ]
      },
      //{ path: 'user/:slug/:tab', meta: {requiresAuth: true}, name: 'user.slug.tab', component: () => import('pages/user/profile/PageUserProfile.vue') },
      { path: '403', component: () => import('pages/Error403.vue') },
      {path: '/authcallback', component: () => import('pages/Authcallback.vue')},
      {path: '/cb/:network', component: () => import('pages/Authcallback.vue')},
    ]
  },

  {
    path: '/user/:slug',
    meta: {requiresAuth: true},
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'collections', name: 'user.slug.collections', component: () => import('pages/user/PageUserCollections.vue') },
    ]
  },

  {
    path: '/parse',
    meta: {requiresAuth: true},
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'movies/:from/:to', name: 'parse.movies', component: () => import('pages/ParseMovies.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('pages/Error404.vue')}
    ]
  }
]

export default routes
