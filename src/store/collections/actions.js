/*
export function someAction (context) {
}
*/
import collections from "../data/collections";
import GraphGuestClient from '../../boot/graphGuestClient'
import GraphAuthClient from '../../boot/graphAuthClient'
import * as graphqg from '../graphql'

export async function getCollectionsList({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.COLLECTION,
      //fetchPolicy: 'network-only',
      variables: data,
    })

    if(response.data.collections.current_page > 1){
      commit('setState', {key: 'collectionsList', value: response.data.collections, push: 'data'});
    }else {
      commit('setState', {key: 'collectionsList', value: response.data.collections});
    }
    return response.data.collections;
  } catch (err) {
    throw err
  }
}
//
export async function getCollectionsUserList({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.COLLECTION_BY_USER,
      //fetchPolicy: 'network-only',
      variables: data,
    })

    if(response.data.collectionsByUser.current_page > 1){
      commit('setState', {key: 'collectionsUsersList', value: response.data.collectionsByUser, push: 'data'});
    }else {
      commit('setState', {key: 'collectionsUsersList', value: response.data.collectionsByUser});
    }
    return response.data.collectionsByUser;
  } catch (err) {
    throw err
  }
}
//
export async function getCollectionSlug({commit}, slug) {
  try {
    const response = await collections.slug();
    commit('setState', {key: 'collectionsList', value: response});
    return response;
  } catch (err) {
    throw err
  }
}
//
export async function getCollectionsSett({commit}) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.COLLECTION_SETT,
      fetchPolicy: 'network-only',
    })
    return response.data.collectionsInSett;

  } catch (err) {
    throw err
  }
}
//
export async function addCollection({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.COLLECTION_ADD,
      variables: data,
    })
    return response.data.collectionAdd;
  } catch (err) {
    throw err
  }
}
//
export async function addCollectionItem({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.COLLECTION_ITEM_ADD,
      variables: data,
    })
    return response.data.collectionItemAdd;
  } catch (err) {
    throw err
  }
}
//
export async function getCollectionsInMain({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.COLLECTION_IN_MAIN,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.collectionsInMain;
  } catch (err) {
    throw err
  }
}

//
export async function removeCollection({commit,dispatch}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.COLLECTION_REMOVE,
      variables: data,
    })
    return response.data.collectionRemove;
  } catch (err) {
    throw err
  }
}
