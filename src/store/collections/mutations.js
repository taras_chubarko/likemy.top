import {updateField} from 'vuex-map-fields';
import { LocalStorage } from 'quasar'

export function updateProjectField (state, field) {
  return updateField(state, field)
}

export function setState(state, data){
  if(data.push !== undefined){
    _.forEach(data.value.data, v => {
      state[data.key][data.push].push(v);
    })
    state[data.key].has_more_pages = data.value.has_more_pages;
    state[data.key].current_page = data.value.current_page;
  }else {
    state[data.key] = data.value;
  }
}

