/*
export function someAction (context) {
}
*/
import collections from "../data/collections";
//
export async function getComments({commit}) {
  try {
    const response = await collections.comments();
    commit('setState', {key: 'comments', value: response});
    return response;
  } catch (err) {
    throw err
  }
}
