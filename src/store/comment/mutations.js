import {updateField} from 'vuex-map-fields';
import { LocalStorage } from 'quasar'

export function updateProjectField (state, field) {
  return updateField(state, field)
}

export function setState(state, data){
  state[data.key] = data.value;
}

