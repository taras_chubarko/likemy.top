import Vue from 'vue'

export default {
  inSite(count = 8) {
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: 'Лучшие <b>боевики</b><br/>и экшены',
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          type: Vue.faker().random.arrayElement(['public', 'private'])
        })
      }
      let data = {};
      data.data = items;
      resolve(data);
    });
    return promise;
  },
  inUsersSite(count = 8) {
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: 'Лучшие <b>боевики</b><br/>и экшены',
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          user: {
            image: Vue.faker().image.avatar()
          }
        })
      }
      let data = {};
      data.data = items;
      resolve(data);
    });
    return promise;
  },
  slug(count = 9) {
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;

      for (step = 0; step < count; step++) {
        items.push({
          name: Vue.faker().commerce.productName(),
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          rating: Vue.faker().random.number(100),
          year: Vue.faker().random.arrayElement([2000,2005,1998,2008,2020,2019])+', ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          country: Vue.faker().address.country()+',' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          description: Vue.faker().lorem.paragraphs(2),
        })
      }

      let data = {};
      data.items = items;
      data.collection = {
        name: Vue.faker().commerce.productName(),
        count: Vue.faker().random.number(100),
        user: {
          image: Vue.faker().image.avatar(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(2)),
        }
      };
      resolve(data);
    });
    return promise;
  },
  comments(count = 10){
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let child = [];
      let step;
      let step2;

      for (step2 = 0; step2 < 5; step2++) {
        child.push({
          user:{
            name: Vue.faker().internet.userName(),
            image: Vue.faker().image.avatar(),
            slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          },
          comment:{
            time: '2 мин',
            date: '14.07.2020',
            comment: Vue.faker().lorem.paragraphs(3),
          }
        });
      }

      for (step = 0; step < count; step++) {
        items.push({
          user:{
            name: Vue.faker().internet.userName(),
            image: Vue.faker().image.avatar(),
            slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          },
          comment:{
            time: '2 мин',
            date: '14.07.2020',
            comment: Vue.faker().lorem.paragraphs(3),
            type: Vue.faker().random.arrayElement(['negative', 'positive', 'neutral']),
            children: child
          },
          movie:{
            name: Vue.faker().commerce.productName(),
            image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
            slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          }
        })
      }

      let data = {};
      data.items = items;
      resolve(data);
    });
    return promise;
  }
}
