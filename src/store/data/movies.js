import Vue from 'vue'

export default {
  slider(slug = 'movies', count = 10) {
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: Vue.faker().commerce.productName(),
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          rating: Vue.faker().random.number(100),
          year: Vue.faker().random.arrayElement([2000,2005,1998,2008,2020,2019])+', ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          country: Vue.faker().address.country()+',' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          description: Vue.faker().lorem.paragraphs(2),
          category: '2020 ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
        })
      }
      let data = {};
      data.items = items;
      let tags = [];
      tags.push({
        name: 'все',
        slug: slug + '/all',
      })
      for (step = 0; step < 5; step++) {
        tags.push({
          name: Vue.faker().lorem.word(),
          slug: slug + '/' + Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }
      data.tags = tags;
      resolve(data);
    });
    return promise;
  },
  sliderCollections(count = 8) {
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: 'Лучшие <b>боевики</b><br/>и экшены',
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }
      let data = {};
      data.items = items;
      resolve(data);
    });
    return promise;
  },
  oneFilm() {
    const promise = new Promise((resolve, reject) => {
      let film = {
        name: Vue.faker().commerce.productName(),
        image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
        rating: Vue.faker().random.number(100),
        year: Vue.faker().random.arrayElement([2000,2005,1998,2008,2020,2019])+', ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
        country: Vue.faker().address.country()+',' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
        slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        description: Vue.faker().lorem.paragraphs(2),
        category: '2020 ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
      }
      let data = {};
      data.items = film;
      resolve(data);
    });
    return promise;
  },
  search() {
    const promise = new Promise((resolve, reject) => {
      let group = [];

      let step;
      let items = [];
      for (step = 0; step < Vue.faker().random.number(10); step++) {
        items.push({
          name: Vue.faker().commerce.productName(),
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          category: '2020 ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }

      let items2 = [];
      for (step = 0; step < Vue.faker().random.number(10); step++) {
        items2.push({
          name: Vue.faker().commerce.productName(),
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          category: '2020 ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }

      group.push({
        name: 'Фильмы',
        results: items,
      })

      group.push({
        name: 'Сериалы',
        results: items2,
      })
      let data = {};

      let sm = [];
      sm.push(items.length);
      sm.push(items2.length);

      data.items = group;
      data.total = _.sum(sm);

      resolve(data);
    });
    return promise;
  },
  movie() {
    const promise = new Promise((resolve, reject) => {
      let film = {
        name: Vue.faker().commerce.productName(),
        image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
        imageBig: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
        year: Vue.faker().random.arrayElement([2000,2005,1998,2008,2020,2019]),
        country: Vue.faker().address.country(),
        slogan: Vue.faker().lorem.words(6),
        video: 'https://www.youtube.com/embed/k3_tw44QsZQ?rel=0',
        category: Vue.faker().commerce.productMaterial(),
        producer: Vue.faker().name.findName(),
        likes:{
          up: Vue.faker().random.number(100),
          down: Vue.faker().random.number(100),
          neutral: Vue.faker().random.number(100),
        },
        description: Vue.faker().lorem.paragraphs(3),
      }
      resolve(film);
    });
    return promise;
  },
  movies(count = 10){
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: Vue.faker().commerce.productName(),
          image: 'http://css.progim.net/img/movies/' + Vue.faker().random.number(6) + '.jpeg',
          rating: Vue.faker().random.number(100),
          year: Vue.faker().random.arrayElement([2000,2005,1998,2008,2020,2019])+', ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          country: Vue.faker().address.country()+',' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
          description: Vue.faker().lorem.paragraphs(2),
          category: '2020 ' + Vue.faker().commerce.productMaterial() + ', ' + Vue.faker().commerce.productMaterial(),
        })
      }
      let data = {};
      data.items = items;
      resolve(data);
    });
    return promise;
  },
  actors(count = 15){
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: Vue.faker().name.findName(),
          image: Vue.faker().image.avatar(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }
      let data = {};
      data.items = items;
      resolve(data);
    });
    return promise;
  }

}
