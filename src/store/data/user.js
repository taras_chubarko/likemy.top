import Vue from 'vue'

export default {
  me(){
    const promise = new Promise((resolve, reject) => {
      let user = {
        name: Vue.faker().internet.userName(),
        image: Vue.faker().image.avatar(),
        slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
      }
      resolve(user);
    });
    return promise;
  },
  preference(){
    const promise = new Promise((resolve, reject) => {
      let user = {
        name: Vue.faker().internet.userName(),
        image: Vue.faker().image.avatar(),
        slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
      }

      let data = {};

      data.ganres = [
        { label: Vue.faker().lorem.words(1), value: 49, color: '#f25a54' },
        { label: Vue.faker().lorem.words(1), value: 31, color: '#6cbdd2' },
        { label: Vue.faker().lorem.words(1), value: 10, color: '#aadd85' },
        { label: Vue.faker().lorem.words(1), value: 10, color: '#fcd616' }
      ];

      resolve(data);
    });
    return promise;
  },
  follows(count = 10){
    const promise = new Promise((resolve, reject) => {
      let items = [];
      let step;
      for (step = 0; step < count; step++) {
        items.push({
          name: Vue.faker().internet.userName(),
          image: Vue.faker().image.avatar(),
          slug: Vue.faker().helpers.slugify(Vue.faker().lorem.words(3)),
        })
      }
      let data = {};
      data.items = items;
      resolve(data);
    });
    return promise;
  }

}
