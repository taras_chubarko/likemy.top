import gql from 'graphql-tag'

const movieShort = gql`fragment movieShort on Movie{id filmID nameRU nameEN description bigPosterURL country genre year slogan rating slug bookMark{type} director{id nameRU} budgetData{budget} rentData{premiereRU premiereWorld}}`


export const LOGIN_MUTATION = gql`mutation($username:String! $password:String! $remember_me:Boolean!){userLogin(username:$username password:$password remember_me:$remember_me){data{token_type token expires_at}user{id name surname login email gender birthday slug bgImage avatarImage}}}`
export const LOGOUT_MUTATION = gql`mutation{userLogout}`
export const REGISTER_MUTATION = gql`mutation($login:String! $email:String! $password:String! $password_confirmation:String!){userRegister(login:$login email:$email password:$password password_confirmation:$password_confirmation){data{token token_type expires_at}user{id name surname login email gender birthday slug bgImage avatarImage}}}`
export const LOGIN_SOCIAL_MUTATION = gql`mutation($data:String!,$network:String!){userSocAuth(data:$data,network:$network){data{token token_type expires_at}user{id name surname login email gender birthday slug bgImage avatarImage}}}`
export const CHECK_REGISTER = gql`mutation($field:String,$name:String){userRegisterCheck(field:$field,name:$name)}`
export const EMAIL_ACTIVATION = gql`mutation($id:Int!){userEmailActivation(id:$id)}`
export const USER_SOC_DATA = gql`mutation($network:String,$code:String,$redirect_uri:String){userSocData(network:$network,code:$code,redirect_uri:$redirect_uri){id name first_name last_name email picture}}`

//export const MOVIES_IN_MAIN = gql`query($take:Int!,$type:String!,$filter:String,$genre:String,$hide:Boolean!){movieList(take:$take,type:$type,filter:$filter,genre:$genre,hide:$hide){tags{name slug}items{...movieShort} info}}${movieShort}`
export const MOVIES_IN_MAIN = gql`query($per_page:Int = 5 $current_page:Int = 1 $type:String! $filter:String $genre:String $hide:Boolean){movieInMain(per_page:$per_page current_page:$current_page type:$type filter:$filter genre:$genre hide:$hide){data{...movieShort}total per_page current_page from to last_page has_more_pages}}${movieShort}`
export const MOVIES_PAGINATE = gql`query($data:MoviesPaginateInput!){moviesPaginate(data:$data){data{...movieShort}total per_page current_page from to last_page has_more_pages}}${movieShort}`
export const MOVIES_FILTER = gql`query{moviesFilter{genre{name}country{name}}}`
export const GET_MOVIE = gql`query($slug:String!){movie(slug:$slug){id filmID nameRU nameEN bigPosterURL country genre year slogan rating ratings{positivePercent negativePercent neutralPercent}kadr description slug comments_statistic{positive neutral negative} videoURL{hd} director{id nameRU} bookMark{type} ratingAgeLimits budgetData{budget} rentData{premiereRU premiereWorld}}}`
export const MOVIE_SIMILAR = gql`query($filmID:Int!){movieSimilar(filmID:$filmID){...movieShort}}${movieShort}`
export const MOVIE_SPF = gql`query($filmID:Int!){movieSequelsAndPrequels(filmID:$filmID){...movieShort}}${movieShort}`
export const MOVIE_FROM_DIRECTOR = gql`query($peopleID:Int!,$filmID:Int!){movieFromDirector(peopleID:$peopleID,filmID:$filmID){...movieShort}}${movieShort}`
export const MOVIE_ACTORS = gql`query($filmID:Int!){movieActors(filmID:$filmID){id nameRU posterURL}}`
export const MOVIE_ADD_LIKE = gql`mutation($movie_id:Int!,$type:String!){movieAddLike(movie_id:$movie_id,type:$type){id rating ratings{negativePercent positivePercent neutralPercent}}}`
export const MOVIE_CHANGE_LIKE = gql`mutation($movie_id:Int!,$type:String!){movieChangeLike(movie_id:$movie_id,type:$type){id rating ratings{negativePercent positivePercent neutralPercent}}}`
export const MOVIE_COMMENT_ADD = gql`mutation($comment:String!,$movie_id:Int!,$like:String){movieCommentAdd(comment:$comment,movie_id:$movie_id,like:$like){data{id comment dt type likes{positive negative} user{avatarImage login slug}children{id parent_id comment dt likes{positive negative} user{avatarImage login slug}}}total per_page current_page from to last_page has_more_pages}}`
export const MOVIE_MY_LIKE = gql`query($movie_id:Int!){movieMyLike(movie_id:$movie_id){type}}`
export const MOVIE_COMMENTS = gql`query($movie_id:Int!,$per_page:Int = 20,$current_page:Int = 1,$sort:String = "date"){movieComments(movie_id:$movie_id per_page:$per_page current_page:$current_page sort:$sort){data{id comment dt type likes{positive negative} user{avatarImage login slug}children{id parent_id comment dt likes{positive negative} user{avatarImage login slug}}}total per_page current_page from to last_page has_more_pages}}`
export const MOVIE_COMMENT_REVIEW_ADD = gql`mutation($parent_id:Int!,$movie_id:Int!,$comment:String!){movieCommentReviewAdd(parent_id:$parent_id movie_id:$movie_id comment:$comment){id parent_id comment dt likes{positive negative} user{avatarImage login slug}}}`
export const MOVIE_COMMENT_LIKE_ADD = gql`mutation($movie_comment_id:Int!,$type:String!){movieCommentAddLike(movie_comment_id:$movie_comment_id,type:$type){positive negative}}`
export const MOVIE_SEARCH = gql`query($search:String!){movieSearch(search:$search){name results{id nameRU bigPosterURL year genre slug}}}`
export const MOVIE_TOP_TODAY = gql`query($type:String!){movieTopDay(type:$type){...movieShort}}${movieShort}`
export const MOVIE_RANDOM = gql`query($type:String!){movieRandom(type:$type){...movieShort}}${movieShort}`
export const MOVIE_ACTOR = gql`query($id:Int!){movieActor(id:$id){nameRU nameEN posterURL birthday age genreStatistic{name percent}}}`
export const MOVIE_ACTOR_FILMS = gql`query($id:Int!,$per_page:Int = 12,$current_page:Int = 1){movieActorFilms(id:$id,per_page:$per_page,current_page:$current_page){data{...movieShort}total per_page current_page from to last_page has_more_pages}}${movieShort}`
export const MOVIE_FF_SEARCH = gql`query($search:String!,$type:String!){moviesFilmPlusFilmSearch(search:$search,type:$type){...movieShort}}${movieShort}`
export const MOVIE_FF= gql`query($f1:Int!,$f2:Int!){moviesFilmPlusFilm(f1:$f1,f2:$f2){...movieShort}}${movieShort}`
export const MOVIE_FF_RANDOM= gql`query($type:String!){moviesFilmPlusFilmRandom(type:$type){...movieShort}}${movieShort}`


export const BOOKMARK_ADD = gql`mutation($movie_id:Int!,$type:String!){bookMarkAdd(movie_id:$movie_id,type:$type){type}}`

export const COLLECTION_SETT = gql`query{collectionsInSett{id name type slug countItems}}`
export const COLLECTION_ADD = gql`mutation($id:Int,$name:String!,$description:String,$type:String!){collectionAdd(id:$id,name:$name,description:$description,type:$type){id name description type slug countItems}}`
export const COLLECTION_ITEM_ADD = gql`mutation($collection_id:Int!,$movie_id:Int!){collectionItemAdd(collection_id:$collection_id,movie_id:$movie_id){id name type countItems}}`
export const COLLECTION_IN_MAIN = gql`query($take:Int = 10){collectionsInMain(take:$take){id name slug posterImage}}`
export const COLLECTION = gql`query($per_page:Int = 8,$current_page:Int = 1){collections(per_page:$per_page,current_page:$current_page){data{id name slug posterImage}total per_page current_page from to last_page has_more_pages}}`
export const COLLECTION_BY_USER = gql`query($per_page:Int = 8,$current_page:Int = 1){collectionsByUser(per_page:$per_page,current_page:$current_page){data{id name slug posterImage user{avatarImage slug}}total per_page current_page from to last_page has_more_pages}}`
export const COLLECTION_REMOVE = gql`mutation($id:Int!){collectionRemove(id:$id)}`

export const USER_BOOKMARK_MENU = gql`query{userBookMarkMenu{name count component type separator}}`
export const USER_BOOKMARK = gql`query($per_page:Int = 20,$current_page:Int = 1,$type:String!){userBookMark(per_page:$per_page,current_page:$current_page,type:$type){data{...movieShort}total per_page current_page from to last_page has_more_pages}}${movieShort}`
export const USER_COLLECTIONS = gql`query($per_page:Int = 20,$current_page:Int = 1){userCollections(per_page:$per_page,current_page:$current_page){data{id name type posterImage slug}total per_page current_page from to last_page has_more_pages}}`
export const USER_COLLECTIONS_ITEMS = gql`query($per_page:Int = 20,$current_page:Int = 1,$slug:String!){userCollectionItems(per_page:$per_page current_page:$current_page slug:$slug){collection{name countItems user{avatarImage slug}}items{data{...movieShort}total per_page current_page from to last_page has_more_pages}}}${movieShort}`
export const USER_PREFERENCES = gql`query($slug:String!,$type:String!){userPreferences(slug:$slug,type:$type){total genre{name count percent}county{name count percent}year{labels percent}actors{id nameRU posterURL}}}`
export const USER_LIKES_COUNT = gql`query($slug:String){userLikesCount(slug:$slug){positive neutral negative}}`
export const USER_LIKES_MOVIE = gql`query($slug:String!,$filter:UseLikesMovieItput){userLikesMovie(slug:$slug,filter:$filter){data{...movieShort}total per_page current_page from to last_page has_more_pages}}${movieShort}`
export const USER_COMMENTS_MOVIE = gql`query($slug:String!,$filter:UseCommentsMovieInput){userCommentsMovie(slug:$slug,filter:$filter){data{id comment dt type likes{positive negative} movie{id bigPosterURL nameRU slug}children{id parent_id comment dt likes{positive negative} user{avatarImage login slug}}}total per_page current_page from to last_page has_more_pages}}`
export const USER_FOLLOW_REQUEST = gql`mutation($slug:String!){userFollowRequest(slug:$slug)}`
export const GET_USER_FOLLOWERS = gql`query($slug:String! $per_page:Int! $current_page:Int! $search:String $sortBy:String $sortField:String){userFollowers(slug:$slug per_page:$per_page current_page:$current_page search:$search sortBy:$sortBy sortField:$sortField){data{id avatarImage slug login likes_count movie_comments_count last_activity(format:"DD MMM YYYY")}total per_page current_page from to last_page has_more_pages}}`
export const GET_USER_FOLLOWINGS = gql`query($slug:String! $per_page:Int! $current_page:Int! $search:String $sortBy:String $sortField:String){userFollowings(slug:$slug per_page:$per_page current_page:$current_page search:$search sortBy:$sortBy sortField:$sortField){data{id avatarImage slug login likes_count movie_comments_count last_activity(format:"DD MMM YYYY")}total per_page current_page from to last_page has_more_pages}}`
export const GET_USER_CHOSEN = gql`query($slug:String! $per_page:Int! $current_page:Int! $search:String $sortBy:String $sortField:String){userChosen(slug:$slug per_page:$per_page current_page:$current_page search:$search sortBy:$sortBy sortField:$sortField){data{id avatarImage slug login likes_count movie_comments_count last_activity(format:"DD MMM YYYY")}total per_page current_page from to last_page has_more_pages}}`
export const GET_USER_DEGREE = gql`query($data:UserDegreeInput){userDegree(data: $data){data{id avatarImage slug login percent_degree likes_count movie_comments_count last_activity(format:"DD MMM YYYY") inFollowers coincidence}total per_page current_page from to last_page has_more_pages}}`
export const GET_PROFILE = gql`query($slug:String!){userProfile(slug:$slug){id name surname login email ifollow gender birthday slug avatarImage bgImage}}`
export const GRT_PROFILE_TABS = gql`query($slug:String!){userProfileTabs(slug:$slug){name path}}`
export const USER_CHOSEN = gql`mutation($slug:String!){userChosen(slug:$slug)}`
export const USER_TIMELINE = gql`query($slug:String! $type:String! $per_page:Int = 2 $current_page:Int = 1){userFeedTimeLine(slug:$slug type:$type per_page:$per_page current_page:$current_page){data{date items{user{avatarImage login slug}count movies{movie{nameRU bigPosterURL id slug}like{type}comment{comment id}}}}total per_page current_page from to last_page has_more_pages}}`


export const FEED_TABS = gql`query($slug:String!){userFeedTabs(slug:$slug){name path}}`


export const MESSAGE_INBOX = gql`query($slug:String! $per_page:Int = 15 $current_page:Int = 1 $search:String){messagesInbox(slug:$slug per_page:$per_page current_page:$current_page search:$search){data{id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject bodyShort updated_at(format:"D MMM")read trash starred}total per_page current_page from to last_page has_more_pages}}`
export const MESSAGE_SENT = gql`query($slug:String! $per_page:Int = 15 $current_page:Int = 1 $search:String){messagesSent(slug:$slug per_page:$per_page current_page:$current_page search:$search){data{id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject bodyShort updated_at(format:"D MMM")read trash starred}total per_page current_page from to last_page has_more_pages}}`
export const MESSAGE_CHATS = gql`query($slug:String! $per_page:Int = 15 $current_page:Int = 1 $search:String){messagesChats(slug:$slug per_page:$per_page current_page:$current_page search:$search){data{id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject bodyShort updated_at(format:"D MMM")read trash starred}total per_page current_page from to last_page has_more_pages}}`
export const MESSAGE_STARRED = gql`query($slug:String! $per_page:Int = 15 $current_page:Int = 1 $search:String){messagesStarred(slug:$slug per_page:$per_page current_page:$current_page search:$search){data{id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject bodyShort updated_at(format:"D MMM")read trash starred}total per_page current_page from to last_page has_more_pages}}`
export const MESSAGE_TRASH = gql`query($slug:String! $per_page:Int = 15 $current_page:Int = 1 $search:String){messagesTrashed(slug:$slug per_page:$per_page current_page:$current_page search:$search){data{id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject bodyShort updated_at(format:"D MMM")read trash starred}total per_page current_page from to last_page has_more_pages}}`
export const MESSAGE = gql`query($id:Int!){messageRead(id:$id){id type sender{id login avatarImage slug last_activity(format:"DD MMM YYYY") time:last_activity(format:"HH:mm")}subject body parent_id owner_id starred read created_at(format:"DD MMM YYYY")time:created_at(format:"HH:mm")}}`
export const MESSAGE_SEND = gql`mutation($data:MessageSendInput){messageSend(data:$data)}`
export const MESSAGE_ADD_STARRED = gql`mutation($message_id:Int!){messageAddStarred(message_id:$message_id)}`
export const MESSAGE_ADD_TRASH = gql`mutation($messages:[Int]){messageAddTrash(messages:$messages)}`
export const MESSAGE_UNREAD = gql`query{messagesUnread}`
export const CHAT_MESSAGES = gql`query($chat_id:Int!,$per_page:Int = 2,$current_page:Int = 1){chatMessges(chat_id:$chat_id per_page:$per_page current_page:$current_page){data{date messages{id message time position sender{avatarImage login}}}total per_page current_page from to last_page has_more_pages}}`
export const CHAT_MESSAGE_ADD = gql`mutation($chat_id:Int!,$message:String!){chatMessageAdd(chat_id:$chat_id,message:$message){date messages{id message time position sender{avatarImage login}}}}`
