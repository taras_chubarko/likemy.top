import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import movies from './movies'
import main from './main'
import collections from './collections'
import comment from './comment'
import user from './user'
import messages from './messages'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
      movies,
      main,
      collections,
      comment,
      user,
      messages
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    //strict: true
  })

  return Store
}
