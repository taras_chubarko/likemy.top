//
import { LocalStorage } from 'quasar'
//
export function setMenuAlways({commit, state}) {
  let data = !state.showTopMenuAlways;
  LocalStorage.set('showTopMenuAlways', data);
  commit('setState', {key: 'showTopMenuAlways', value: data});
  commit('setState', {key: 'showTopMenu', value: !state.showTopMenu});
}
