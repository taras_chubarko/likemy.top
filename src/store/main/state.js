import { LocalStorage } from 'quasar'

export default function () {
  return {
    //
    showTopMenu: false,
    showTopMenuAlways: LocalStorage.has('showTopMenuAlways') ? LocalStorage.getItem('showTopMenuAlways') : false,
  }
}
