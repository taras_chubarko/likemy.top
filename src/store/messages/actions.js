import GraphAuthClient from '../../boot/graphAuthClient';
import * as graphqg from '../graphql';

export async function getInbox({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_INBOX,
      variables: data,
      //fetchPolicy: data.cache !== undefined ? data.cache : 'cache-first',
      fetchPolicy: 'network-only',
    });

    if (response.data.messagesInbox.current_page > 1) {
      commit('setState', {
        key: 'list',
        value: response.data.messagesInbox,
        push: 'data',
      });
    } else {
      commit('setState', {
        key: 'list',
        value: response.data.messagesInbox,
      });
    }
    return response.data.messagesInbox;
  } catch (err) {
    throw err;
  }
}

export async function getSent({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_SENT,
      variables: data,
      //fetchPolicy: data.cache !== undefined ? data.cache : 'cache-first',
      fetchPolicy: 'network-only',
    });

    if (response.data.messagesSent.current_page > 1) {
      commit('setState', {
        key: 'list',
        value: response.data.messagesSent,
        push: 'data',
      });
    } else {
      commit('setState', {
        key: 'list',
        value: response.data.messagesSent,
      });
    }
    return response.data.messagesSent;
  } catch (err) {
    throw err;
  }
}

export async function getChats({commit, state}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_CHATS,
      variables: data,
      //fetchPolicy: data.cache !== undefined ? data.cache : 'cache-first',
      fetchPolicy: 'network-only',
    });

    if (response.data.messagesChats.current_page > 1) {
      if (response.data.messagesChats.current_page !==
          response.data.messagesChats.last_page) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesChats,
          push: 'data',
        });
      }

      if (response.data.messagesChats.current_page ===
              response.data.messagesChats.last_page &&
          state.list.data.length !== response.data.messagesChats.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesChats,
          push: 'data',
        });
      } else if (
          response.data.messagesChats.data.length >=
          response.data.messagesChats.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesChats,
        });
      }
    } else {
      commit('setState', {
        key: 'list',
        value: response.data.messagesChats,
      });
    }

    return response.data.messagesChats;
  } catch (err) {
    throw err;
  }
}

export async function getStarred({commit, state}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_STARRED,
      variables: data,
      //fetchPolicy: data.cache !== undefined ? data.cache : 'cache-first',
      fetchPolicy: 'network-only',
    });

    if (response.data.messagesStarred.current_page > 1) {
      if (response.data.messagesStarred.current_page !==
          response.data.messagesStarred.last_page) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesStarred,
          push: 'data',
        });
      }

      if (response.data.messagesStarred.current_page ===
              response.data.messagesStarred.last_page &&
          state.list.data.length !== response.data.messagesStarred.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesStarred,
          push: 'data',
        });
      } else if (
          response.data.messagesStarred.data.length >=
          response.data.messagesStarred.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesStarred,
        });
      }
    } else {
      commit('setState', {
        key: 'list',
        value: response.data.messagesStarred,
      });
    }

    return response.data.messagesStarred;
  } catch (err) {
    throw err;
  }
}

export async function getTrashed({commit, state}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_TRASH,
      variables: data,
      //fetchPolicy: data.cache !== undefined ? data.cache : 'cache-first',
      fetchPolicy: 'network-only',
    });

    if (response.data.messagesTrashed.current_page > 1) {
      if (response.data.messagesTrashed.current_page !==
          response.data.messagesTrashed.last_page) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesTrashed,
          push: 'data',
        });
      }

      if (response.data.messagesTrashed.current_page ===
              response.data.messagesTrashed.last_page &&
          state.list.data.length !== response.data.messagesTrashed.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesTrashed,
          push: 'data',
        });
      } else if (
          response.data.messagesTrashed.data.length >=
          response.data.messagesTrashed.total) {
        commit('setState', {
          key: 'list',
          value: response.data.messagesTrashed,
        });
      }
    } else {
      commit('setState', {
        key: 'list',
        value: response.data.messagesTrashed,
      });
    }

    return response.data.messagesTrashed;
  } catch (err) {
    throw err;
  }
}

export async function getMessage({commit, state}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE,
      variables: data,
      // fetchPolicy: 'network-only',
    });

    commit('setState', {
      key: 'message',
      value: response.data.messageRead,
    });

    if (state.list && state.list.data && state.list.data.length !== 0) {
      state.list.data.forEach(v => {
        if (v.id === response.data.messageRead.id) {
          v.read = response.data.messageRead.read;
        }
      });
    }

    return response.data.messageRead;
  } catch (err) {
    throw err;
  }
}

export async function getMessagesUnread({commit, state}) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_UNREAD,
    });

    commit('setState', {
      key: 'messagesUnread',
      value: response.data.messagesUnread,
    });

    return response.data.messagesUnread;
  } catch (err) {
    throw(err);
  }
}

export async function messageSend({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MESSAGE_SEND,
      variables: {data: data},
    });
    GraphAuthClient.resetStore();
    return response.data.messageSend;
  } catch (err) {
    throw err;
  }
}

export async function messageAddStarred({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MESSAGE_ADD_STARRED,
      variables: data,
    });
    GraphAuthClient.resetStore();
    return response.data.messageAddStarred;
  } catch (err) {
    throw err;
  }
}

export async function messageAddTrash({commit, state}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MESSAGE_ADD_TRASH,
      variables: {messages: data},
    });

    commit('setState', {
      key: 'list',
      value: [],
      push: 'data',
    });

    return response.data.messageAddTrash;
  } catch (err) {
    throw err;
  }
}

export async function getUnreadMessages({commit, state}) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MESSAGE_UNREAD,
      // variables: data,
      fetchPolicy: 'network-only',
    });
    return response.data.messagesUnread;
  } catch (err) {
    throw err;
  }
}

export async function getChatMessages({commit, state}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.CHAT_MESSAGES,
      variables: data,
      fetchPolicy: 'network-only',
    });
    commit('setChatMessages', response.data.chatMessges);
    return response.data.chatMessges;
  } catch (err) {
    throw err;
  }
}

export async function messageAdd({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.CHAT_MESSAGE_ADD,
      variables: data,
    });
    // commit('addChatMessages', response.data.chatMessageAdd);
    return response.data.chatMessageAdd;
  } catch (err) {
    throw err;
  }
}
