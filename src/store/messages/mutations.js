import {LocalStorage} from 'quasar'
import {updateField} from 'vuex-map-fields';

export function updateProjectField(state, field) {
  return updateField(state, field)
}

export function setState(state, data) {
  if (data.push !== undefined) {
    _.forEach(data.value.data, v => {
      state[data.key][data.push].push(v);
    });
    state[data.key].has_more_pages = data.value.has_more_pages;
    state[data.key].current_page = data.value.current_page;
  } else {
    state[data.key] = data.value;
  }
}

export function setChatMessages(state, data) {
  state.chatMessages.current_page = data.current_page;
  state.chatMessages.has_more_pages = data.has_more_pages;

  if (state.chatMessages.current_page === 1) {
    state.chatMessages.data = [];
  }

  if (data.data.length !== 0) {
    data.data.forEach(v => {
      state.chatMessages.data.unshift(v);
    });
  }
}

export function addChatMessages(state, data) {
  if (state.chatMessages.data.length) {
    if (data.date === 'Сегодня' &&
        !state.chatMessages.data.find(d => d.date === 'Сегодня')) {
      state.chatMessages.data.push({
        date: 'Сегодня',
        messages: [],
      });
    }

    _.forEach(state.chatMessages.data, (v, k) => {
      if (v.date === data.date) {
        _.forEach(data.messages, m => {
          state.chatMessages.data[k].messages.push(m);
        });
      }
    });
  } else {
    state.chatMessages.data.push(data);
  }
}
