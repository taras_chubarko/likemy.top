export default function () {
  return {
    //
    list:{},
    message:{},
    open: false,
    messagesUnread: 0,
    chatMessages: {
      data: [],
    },
  }
}
