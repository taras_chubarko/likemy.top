import Vue from 'vue'
import movies from '../data/movies'
import { LocalStorage } from 'quasar'
import GraphGuestClient from '../../boot/graphGuestClient'
import GraphAuthClient from '../../boot/graphAuthClient'
import * as graphqg from '../graphql'

export async function getSlideMovies({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIES_IN_MAIN,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.movieInMain;

  } catch (err) {
    throw err
  }
}
//
export async function getSlideCollections({commit}) {
  try {
    const response = await movies.sliderCollections();
    return response;

  } catch (err) {
    throw err
  }
}
//
export async function getOneFilm({commit}) {
  try {
    const response = await movies.oneFilm();
    return response;

  } catch (err) {
    throw err
  }
}
//
export async function searchMovies({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_SEARCH,
      variables: data,
      //fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })
    return response.data.movieSearch;
  } catch (err) {
    throw err
  }
}
//
export async function getMovie({commit, dispatch}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.GET_MOVIE,
      variables: {slug: data.slug},
      fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })
    commit('setState', {key: 'movie', value: response.data.movie});
    dispatch('getMovieComments', {movie_id: response.data.movie.id})
    return response.data.movie;
  } catch (err) {
    throw err
  }
}
//
export function setGridList({commit}, data) {
  //let data = state.gridList === 'grid' ? 'list' : 'grid';
  LocalStorage.set('gridList', data);
  commit('setState', {key: 'gridList', value: data});
}
//
export async function getMovies({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIES_PAGINATE,
      variables: data,
      fetchPolicy: 'network-only',
    })

    if(response.data.moviesPaginate.current_page > 1){
      commit('setState', {key: 'movies', value: response.data.moviesPaginate, push: 'data'});
    }else {
      commit('setState', {key: 'movies', value: response.data.moviesPaginate});
    }

    return response.data.moviesPaginate;
  } catch (err) {
    throw err
  }
}

export function updateF({commit}, data) {
  commit('setState', {key: data['key'], value:data['value']});
}
//
export async function getMoviesFilter({commit}) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIES_FILTER,
    })
    return response.data.moviesFilter;
  } catch (err) {
    throw err
  }
}
//
export async function getMoviesSimilar({commit}, filmID) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_SIMILAR,
      variables: {filmID:filmID},
    })
    return response.data.movieSimilar;
  } catch (err) {
    throw err
  }
}
//
export async function getMoviesSequelsAndPrequels({commit}, filmID) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_SPF,
      variables: {filmID:filmID},
    })
    return response.data.movieSequelsAndPrequels;
  } catch (err) {
    throw err
  }
}
//
export async function getMoviesFromDirector({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_FROM_DIRECTOR,
      variables: data,
    })
    return response.data.movieFromDirector;
  } catch (err) {
    throw err
  }
}
//
export async function getMoviesActors({commit}, filmID) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_ACTORS,
      variables: {filmID:filmID},
    })
    return response.data.movieActors;
  } catch (err) {
    throw err
  }
}

//
export async function addMovieLike({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MOVIE_ADD_LIKE,
      variables: data,
    })
    return response.data.movieAddLike;
  } catch (err) {
    throw err
  }
}
//
export async function changeMovieLike({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MOVIE_CHANGE_LIKE,
      variables: data,
    })
    return response.data.movieChangeLike;
  } catch (err) {
    throw err
  }
}
//
export async function addMovieComment({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MOVIE_COMMENT_ADD,
      variables: data,
    })
    commit('setState', {key: 'comments', value: response.data.movieCommentAdd});
    return response.data.movieCommentAdd;
  } catch (err) {
    throw err
  }
}
//
export async function getMyLike({commit}, movie_id) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.MOVIE_MY_LIKE,
      variables: {movie_id:movie_id},
    })
    return response.data.movieMyLike;
  } catch (err) {
    throw err
  }
}
//
export async function getMovieComments({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_COMMENTS,
      variables: data,
      fetchPolicy: 'network-only',
    })
    commit('setState', {key: 'comments', value: response.data.movieComments});
    return response.data.movieComments;
  } catch (err) {
    throw err
  }
}
//
export async function addMovieCommentReview({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MOVIE_COMMENT_REVIEW_ADD,
      variables: data,
    })
    commit('addCommentReview', response.data.movieCommentReviewAdd);
    return response.data.movieCommentReviewAdd;
  } catch (err) {
    throw err
  }
}
//
export async function addMovieCommentLikeAdd({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.MOVIE_COMMENT_LIKE_ADD,
      variables: data,
    })
    //commit('addCommentReview', response.data.movieCommentReviewAdd);
    return response.data.movieCommentAddLike;
  } catch (err) {
    throw err
  }
}
//
export async function bookMarkAdd({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.BOOKMARK_ADD,
      variables: data,
    })
    //commit('addCommentReview', response.data.movieCommentReviewAdd);
    return response.data.bookMarkAdd;
  } catch (err) {
    throw err
  }
}
//
export async function getMovieTopDay({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_TOP_TODAY,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.movieTopDay;
  } catch (err) {
    throw err
  }
}
//
export async function getMovieRandom({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_RANDOM,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.movieRandom;
  } catch (err) {
    throw err
  }
}
//
export async function getMovieActor({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_ACTOR,
      variables: data,
      //fetchPolicy: 'network-only',
    })
    commit('setState', {key: 'actor', value: response.data.movieActor});
    return response.data.movieActor;
  } catch (err) {
    throw err
  }
}
//
export async function getMovieActorFilms({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_ACTOR_FILMS,
      variables: data,
      //fetchPolicy: 'network-only',
    })
    commit('setState', {key: 'movies', value: response.data.movieActorFilms});
    return response.data.movieActorFilms;
  } catch (err) {
    throw err
  }
}

//
export async function getMovieFFSearch({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_FF_SEARCH,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.moviesFilmPlusFilmSearch;
  } catch (err) {
    throw err
  }
}

//
export async function getMovieFF({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_FF,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.moviesFilmPlusFilm;
  } catch (err) {
    throw err
  }
}

//
export async function getMovieFFRandom({commit}, data) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.MOVIE_FF_RANDOM,
      variables: data,
      fetchPolicy: 'network-only',
    })
    return response.data.moviesFilmPlusFilmRandom;
  } catch (err) {
    throw err
  }
}
