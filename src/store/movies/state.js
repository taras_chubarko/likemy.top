export default function () {
  return {
    //
    movie: null,
    movies: {},
    comments: null,
    gridList: 'grid',
    filterForm:{
      per_page: 21,
      type: 'films',
      filter: 'all',
      hideWithLikes: false,
      year:{
        from: null,
        to: null,
      },
      rating: {
        min: 0,
        max: 100
      },
      genre:['all'],
      country: ['all'],
      g: null,
    },
    actor:{}
  }
}
