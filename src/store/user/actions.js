import user from '../data/user'
import GraphGuestClient from '../../boot/graphGuestClient'
import GraphAuthClient from '../../boot/graphAuthClient'
import * as graphqg from '../graphql'

export async function getProfile({commit}, slug) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GET_PROFILE,
      variables: {slug: slug},
      //fetchPolicy: 'network-only',
    })

    commit('setState', {key: 'profile', value: response.data.userProfile});
    return response.data.userProfile;

  } catch (err) {
    throw err
  }
}

//
export async function auth({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.LOGIN_MUTATION,
      variables: data,
    })

    commit('setState', {key: 'token', value: response.data.userLogin.data, store: true});
    commit('setState', {key: 'user', value: response.data.userLogin.user, store: true});
    return response.data.userLogin;
  } catch (err) {
    throw err
  }
}

//
export async function logout({commit}) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.LOGOUT_MUTATION,
    })

    commit('setState', {key: 'token', value: {}, store: false});
    commit('setState', {key: 'user', value: {}, store: false});
    return response.data.userLogout;
  } catch (err) {
    throw err
  }
}
//
export async function getProfileTabs({commit}, slug) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GRT_PROFILE_TABS,
      variables: {slug: slug},
      fetchPolicy: 'network-only',
    })

    commit('setState', {key: 'tabs', value: response.data.userProfileTabs});
    return response.data.userProfileTabs;

  } catch (err) {
    throw err
  }
}
//
export async function register({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.REGISTER_MUTATION,
      variables: data,
    })

    commit('setState', {key: 'token', value: response.data.userRegister.data, store: true});
    commit('setState', {key: 'user', value: response.data.userRegister.user, store: true});
    return response.data.userRegister;
  } catch (err) {
    throw err
  }
}

//
export async function loginSocial({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.LOGIN_SOCIAL_MUTATION,
      variables: data,
    })
    commit('setState', {key: 'token', value: response.data.userSocAuth.data, store: true});
    commit('setState', {key: 'user', value: response.data.userSocAuth.user, store: true});
    return response.data.userSocAuth;
  } catch (err) {
    throw err
  }
}
//
export async function getFollowers({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GET_USER_FOLLOWERS,
      variables: data,
      fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })

    commit('setState', {key: 'followers', value: response.data.userFollowers});
    return response.data.userFollowers;

  } catch (err) {
    throw err
  }
}
//
export async function getFollowings({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GET_USER_FOLLOWINGS,
      variables: data,
      fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })

    commit('setState', {key: 'followers', value: response.data.userFollowings});
    return response.data.userFollowings;

  } catch (err) {
    throw err
  }
}
//
//
export async function getChosen({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GET_USER_CHOSEN,
      variables: data,
      fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })

    commit('setState', {key: 'followers', value: response.data.userChosen});
    return response.data.userChosen;

  } catch (err) {
    throw err
  }
}
//
export async function getDegree({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.GET_USER_DEGREE,
      variables: {data: data},
      fetchPolicy: data.fetchPolicy ? data.fetchPolicy : 'cache-first',
    })

    commit('setState', {key: 'followers', value: response.data.userDegree});
    return response.data.userDegree;

  } catch (err) {
    throw err
  }
}
//

export async function checkRegister({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.CHECK_REGISTER,
      variables: data,
    })
    return response.data.userRegisterCheck;
  } catch (err) {
    throw err
  }
}

//
export async function getBookMarkMenu({commit}, [fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_BOOKMARK_MENU,
      fetchPolicy: fetchPolicy,
    })

    return response.data.userBookMarkMenu;
  } catch (err) {
    throw err
  }
}

//
export async function getBookMark({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_BOOKMARK,
      variables: data,
    })
    commit('setBookmarks', response.data.userBookMark);
    return response.data.userBookMark;
  } catch (err) {
    throw err
  }
}
//
export async function getCollections({commit}, [data, fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_COLLECTIONS,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    commit('setCollections', response.data.userCollections);
    return response.data.userCollections;
  } catch (err) {
    throw err
  }
}
//
export async function getCollectionsItems({commit}, [data, fetchPolicy = 'network-only']) {
  try {
    const response = await GraphGuestClient.query({
      query: graphqg.USER_COLLECTIONS_ITEMS,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    commit('setCollectionsItems', response.data.userCollectionItems);
    return response.data.userCollectionItems;
  } catch (err) {
    throw err
  }
}

//
export async function getPreferences({commit}, [data, fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_PREFERENCES,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    return response.data.userPreferences;
  } catch (err) {
    throw err
  }
}

//
export async function getLikesCount({commit}, [data, fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_LIKES_COUNT,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    return response.data.userLikesCount;
  } catch (err) {
    throw err
  }
}

//
export async function getLikesMovie({commit}, [data, fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_LIKES_MOVIE,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    commit('setMovies', response.data.userLikesMovie);
    return response.data.userLikesMovie;
  } catch (err) {
    throw err
  }
}

//
export async function getCommentsMovie({commit}, [data, fetchPolicy = 'cache-first']) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_COMMENTS_MOVIE,
      variables: data,
      fetchPolicy: fetchPolicy,
    })
    commit('setComments', response.data.userCommentsMovie);
    return response.data.userCommentsMovie;
  } catch (err) {
    throw err
  }
}

//
export async function followReq({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.USER_FOLLOW_REQUEST,
      variables: data,
    })
    return response.data.userFollowRequest;
  } catch (err) {
    throw err
  }
}

//
export async function chosenReq({commit}, data) {
  try {
    const response = await GraphAuthClient.mutate({
      mutation: graphqg.USER_CHOSEN,
      variables: data,
    })
    return response.data.userChosen;
  } catch (err) {
    throw err
  }
}

//
export async function getFeedTabs({commit}, slug) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.FEED_TABS,
      variables: {slug: slug},
      fetchPolicy: 'network-only',
    })

    commit('setState', {key: 'tabs', value: response.data.userFeedTabs});
    return response.data.userFeedTabs;

  } catch (err) {
    throw err
  }
}
//
export async function getFeedTimeLine({commit}, data) {
  try {
    const response = await GraphAuthClient.query({
      query: graphqg.USER_TIMELINE,
      variables: data,
      fetchPolicy: 'network-only',
    })

    commit('setFeeds', response.data.userFeedTimeLine);
    return response.data.userFeedTimeLine;

  } catch (err) {
    throw err
  }
}

//
export async function mailActivation({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.EMAIL_ACTIVATION,
      variables: data,
    })
    return response.data.userEmailActivation;
  } catch (err) {
    throw err
  }
}

//
export async function userSocData({commit}, data) {
  try {
    const response = await GraphGuestClient.mutate({
      mutation: graphqg.USER_SOC_DATA,
      variables: data,
    })
    return response.data.userSocData;
  } catch (err) {
    throw err
  }
}
