import {updateField} from 'vuex-map-fields';
import { LocalStorage } from 'quasar'

export function updateProjectField (state, field) {
  return updateField(state, field)
}

export function setState(state, data){
  state[data.key] = data.value;
  if(data.store !== undefined && data.store === true){
    LocalStorage.set(data.key, data.value);
  }
  if(data.store !== undefined && data.store === false){
    LocalStorage.remove(data.key);
  }
}

export function setBookmarks(state, data) {
  if(data && data.current_page > 1){
    data.data.forEach(v => {
      state.bookmarks.data.push(v);
    })
  }else {
    state.bookmarks = data;
  }

}

export function setCollections(state, data) {
  if(data && data.current_page > 1){
    data.data.forEach(v => {
      state.collections.data.push(v);
    })
  }else {
    state.collections = data;
  }
}

export function setCollectionsItems(state, data) {
  if(data && data.items.current_page > 1){
    data.items.data.forEach(v => {
      state.collectionsItems.items.data.push(v);
    })
  }else {
    state.collectionsItems = data;
  }
}

export function setMovies(state, data) {
  if(data && data.current_page > 1){
    data.data.forEach(v => {
      state.movies.data.push(v);
    })
  }else {
    state.movies = data;
  }
}

export function setComments(state, data) {
  if(data && data.current_page > 1){
    data.data.forEach(v => {
      state.comments.data.push(v);
    })
  }else {
    state.comments = data;
  }
}

export function setFeeds(state, data) {
  state.feed.current_page = data.current_page;
  state.feed.has_more_pages = data.has_more_pages;
  if(Object.keys(data).length && data.current_page >= 2){
    data.data.forEach(v => {
      state.feed.data.push(v);
    })
  }else {
    state.feed = data;
  }
}



