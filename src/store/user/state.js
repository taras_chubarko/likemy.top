import { LocalStorage } from 'quasar'

export default function () {
  return {
    //
    user: LocalStorage.has('user') ? LocalStorage.getItem('user') : {},
    token: LocalStorage.has('token') ? LocalStorage.getItem('token') : {},
    profile: {},
    tabs: [],
    followers: {},
    bookmarks: {},
    collections: {},
    collectionsItems: {},
    movies:{},
    comments:{},
    feed:{},
  }
}
